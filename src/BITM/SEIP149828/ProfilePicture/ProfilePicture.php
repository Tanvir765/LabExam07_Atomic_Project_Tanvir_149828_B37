<?php
namespace App\ProfilePicture;
use App\Model\Database as DB;
use App\Message\Message;
use App\Utility\Utility;
use PDO;

class ProfilePicture extends DB
{
    public $id = "";
    public $name = "";
    public $picture_address = "";

    public function __construct()
    {
        parent::__construct();
        echo "<br>";
    }

    public function setData($data = NULL)
    {
        if (array_key_exists("id", $data)) {
            $this->id = $data["id"];
        }
        if (array_key_exists("name", $data)) {
            $this->name = $data["name"];
        }
        if (array_key_exists("picture_address", $data)) {
            $this->picture_address = $data["picture_address"];
        }
    }

    public function store()
    {
        $dbh = $this->connection;
        $values = array($this->name, $this->picture_address);
        $query = "insert into profile_picture(name,picture_address) VALUES (?,?)";
        $sth = $dbh->prepare($query);
        $result = $sth->execute($values);

        if ($result) {
            Message::message("<div id='msg'></div><h3 align='center'>[ Name: $this->name ] , [ picture_address: $this->picture_address ] <br> Data Has Been Inserted Successfully!</h3></div>");
        } else {
            Message::message("<div id='msg'></div><h3 align='center'>[ Name: $this->name ] , [ picture_address: $this->picture_address ] <br> Data Has Not Been Inserted Successfully!</h3></div>");
        }

        Utility::redirect('create.php');


    }

//include_once("../../../src/BITM/SEIP_128750/BookTitle/BookTitle.php");

//use App\BookTitle;

    public function upload()
    {
        $obProPic = new ProfilePicture();
        $filename = $_FILES['picture_address']['tmp_name'];

        $located_folder = '../../../resource/ProfilePic/';

        $uploaded_file = time() . $_FILES['picture_address']['name'];
        $destination = $located_folder . $uploaded_file;
        move_uploaded_file($filename, $destination);
        $_POST['picture_address'] = $uploaded_file;
//var_dump($_POST);
        $obProPic->setData($_POST);
        $obProPic->store();


    }
}

?>
