<?php
namespace App\Hobbies;
use App\Model\Database as DB;
use App\Message\Message;
use App\Utility\Utility;
use PDO;

class Hobbies extends DB
{
    public $id = "";
    public $name = "";
    public $hobbies = "";

    public function __construct()
    {
        parent::__construct();
        echo "<br>";
    }

    public function setData($data = NULL)
    {
        if (array_key_exists("id", $data)) {
            $this->id = $data["id"];
        }
        if (array_key_exists("name", $data)) {
            $this->name = $data["name"];
        }
        if (array_key_exists("hobbies", $data)) {
            $hobby=implode(',',$data['hobbies']);
            $this->hobbies = $hobby;
        }
    }

    public function store()
    {
        $dbh = $this->connection;
        $values = array($this->name, $this->hobbies);
        $query = "insert into hobbies(name,hobbies) VALUES (?,?)";
        $sth = $dbh->prepare($query);
        $result = $sth->execute($values);

        if ($result) {
            Message::message("<div id='msg'></div><h3 align='center'>[ Name: $this->name ] , [ hobbies: $this->hobbies ] <br> Data Has Been Inserted Successfully!</h3></div>");
        } else {
            Message::message("<div id='msg'></div><h3 align='center'>[ Name: $this->name ] , [ hobbies: $this->hobbies ] <br> Data Has Not Been Inserted Successfully!</h3></div>");
        }

        Utility::redirect('create.php');


    }
}