<?php
namespace App\BookTitle;
use App\Message\Message;
use App\Utility\Utility;
use App\Model\Database as DB;

class BookTitle extends DB{
    public $id = "";
    public $book_title = "";
    public $author_name = "";
    public function __construct()
    {
        parent::__construct();
    }
    public function setData($data=NULL){
        if(array_key_exists("id",$data)){
            $this->id=$data["id"];
        }
        if(array_key_exists("book_title",$data)){
            $this->book_title=$data["book_title"];
        }
        if(array_key_exists("author_name",$data)){
            $this->author_name=$data["author_name"];
        }
    }
    public function store(){
        $dbh=$this->connection;
        $values=array($this->book_title,$this->author_name);
        $query="insert into book_title(book_title,author_name) VALUES (?,?)";
        $sth=$dbh->prepare($query);
       $result= $sth->execute($values);

        if($result){
            Message::message("<div id='msg'></div><h3 align='center'>[ BookTitle: $this->book_title ] , [ AuthorName: $this->author_name ] <br> Data Has Been Inserted Successfully!</h3></div>");
        }else{
            Message::message("<div id='msg'></div><h3 align='center'>[ BookTitle: $this->book_title ] , [ AuthorName: $this->author_name ] <br> NO Data Has Been Inserted!!!!!!</h3></div>");

        }

        Utility::redirect('create.php');


    }
}
