<?php
namespace App\SummaryOfOrganization;
use App\Model\Database as DB;
use App\Message\Message;
use App\Utility\Utility;
use PDO;

class SummaryOfOrganization extends DB
{
    public $id = "";
    public $organization_name = "";
    public $organization_summary = "";

    public function __construct()
    {
        parent::__construct();
        echo "<br>";
    }

    public function setData($data = NULL)
    {
        if (array_key_exists("id", $data)) {
            $this->id = $data["id"];
        }
        if (array_key_exists("organization_name", $data)) {
            $this->organization_name = $data["organization_name"];
        }
        if (array_key_exists("organization_summary", $data)) {
            $this->organization_summary = $data["organization_summary"];
        }
    }

    public function store()
    {
        $dbh = $this->connection;
        $values = array($this->organization_name, $this->organization_summary);
        $query = "insert into summary_of_organization(organization_name,organization_summary) VALUES (?,?)";
        $sth = $dbh->prepare($query);
        $result = $sth->execute($values);

        if ($result) {
            Message::message("<div id='msg'></div><h3 align='center'>[ organization_name: $this->organization_name ] , [ organization_summary: $this->organization_summary ] <br> Data Has Been Inserted Successfully!</h3></div>");
        } else {
            Message::message("<div id='msg'></div><h3 align='center'>[ organization_name: $this->organization_name ] , [ organization_summary: $this->organization_summary ] <br> Data Has Not Been Inserted Successfully!</h3></div>");
        }

        Utility::redirect('create.php');


    }
}