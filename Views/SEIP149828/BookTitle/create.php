<?php
require_once("../../../vendor/autoload.php");
use App\Message\Message;

if(!isset( $_SESSION)) session_start();
echo Message::message();

?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Book Title</title>
    <link rel="stylesheet" href="../../../resource/Bootstrap/css/bootstrap.min.css">

    <!-- Optional theme -->
    <link rel="stylesheet" href="../../../resource/Bootstrap/css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="../../../resource/Bootstrap/css/booktitle.css" type="text/css">
</head>
<body>
<div class="container">
    <div class="row vertical-offset-100">
        <div class="col-md-4 col-md-offset-4">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="panel-heading">
                        <h1>Add BookTile</h1>
                        <div class="row-fluid user-row">
                            <img src="../../../resource/images/book.jpg" class="img-responsive icon" alt="Conxole Admin"/>
                            <style>body{
                                    background-image:url("../../../resource/images/book_bag.jpg");
                                    background-repeat: repeat-x;
                                }</style>
                        </div>
                    </div>

                </div>
                <div class="panel-body">
                    <form accept-charset="UTF-8" role="form" class="form-booktitle" method="Post" action="store.php">
                        <fieldset>
                            <label class="panel-input">
                                <div class="input_result"></div>
                            </label>
                            <label class="" for="name">Your Name</label>
                            <input class="form-control" placeholder="Enter Book Name" name="book_title" type="text">
                            <label class="" for="name">Author Name</label>
                            <input class="form-control" placeholder="Enter Author Name " name="author_name" type="text">
                            </br>
                            <input class="btn btn-lg btn-success btn-block" type="submit" name="submit" value="Submit">
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>