<?php
require_once("../../../vendor/autoload.php");
use App\Message\Message;

if(!isset( $_SESSION)) session_start();
echo Message::message();

?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Add Summery Of Organization</title>
    <link rel="stylesheet" href="../../../resource/Bootstrap/css/bootstrap.min.css">

    <!-- Optional theme -->
    <link rel="stylesheet" href="../../../resource/Bootstrap/css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="../../../resource/Bootstrap/css/booktitle.css" type="text/css">
</head>
<body>
<div class="container">
    <div class="row vertical-offset-100">
        <div class="col-md-4 col-md-offset-4">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="panel-heading">
                        <h3>Create Summery of Organization</h3>
                        <div class="row-fluid user-row">
                            <img src="../../../resource/images/org.jpg" class="img-responsive icon" alt="Conxole Admin"/>
                            <style>body{
                                    background-image:url("../../../resource/images/org_bag.jpg");
                                    background-repeat: repeat-x;
                                }</style>
                        </div>
                    </div>

                </div>
                <div class="panel-body">
                    <form accept-charset="UTF-8" role="form" class="form-city" method="Post" action="store.php">
                        <fieldset>
                            <label class="panel-input">
                                <div class="input_result"></div>
                            </label>
                            <label class="" for="name">Name of the organization</label>
                            <input class="form-control" placeholder="name of the organization.." name="organization_name" type="text">
                            <label>Summery:</label>
                            <div class="form-group">

                                <div class="col-sm-4">
                                    <textarea name="organization_summary" class="" rows="6" cols="35" from="organization_summary"></textarea>        </div>
                            </div>
                            <br>
                            <br>
                            <br>
                            <input class="btn btn-lg btn-success btn-block" type="submit" name="submit" value="Submit">
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>